package de.packetcloud.r.meetme;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.SimpleLocationOverlay;

import java.text.DateFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

public class MeetActivity extends FragmentActivity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    private final ArrayList<OverlayItem> overlayItemArray = new ArrayList<>();
    private LocationAvailability mLocationAvailability;
    private float mCurrentAccurancy = 10F;
    private GoogleApiClient mGoogleApiClient = null;
    private GeoPoint mCurrentLocation = null;
    private LocationRequest mLocationRequest = null;
    private boolean mRequestingLocationUpdates = true;
    private boolean mLocationTracking = true;
    private String mLastUpdateTime = "0";
    private MapView mapView = null;
    private IMapController mapController = null;
    private boolean mResolvingError = false;
    private Button buttonGPS;
    private Button buttonTracking;
    private float mCurrentSpeed = 0F;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_meet);
        //gcm = GoogleCloudMessaging.getInstance();
        Locale currentLocale = getResources().getConfiguration().locale;
        String country = "";
        if (currentLocale != null) {
            country = currentLocale.getCountry();
        }

        mapView = (MapView) this.findViewById(R.id.mapview);

        if (mapView == null) {
            setContentView(R.layout.activity_error);
        } else {

            mapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);
            mapView.setBuiltInZoomControls(true);
            mapView.setMultiTouchControls(true);
            mapView.setClickable(true);
            mapView.setDrawingCacheEnabled(true);
            mapView.setMaxZoomLevel(18);
            mapView.setMinZoomLevel(4);
            mapController = mapView.getController();

            if (country.equals("DE")) {
                GeoPoint gp = new GeoPoint((int) (52.5177f * 1E6), (int) (13.3999f * 1E6));
                mapController.setCenter(gp);
            }

            mapController.setZoom(8);

        }
        loadInstance();

        createHighLocationRequest();
        buildGoogleApiClient();
        buttonGPS = (Button) findViewById(R.id.buttonGPS);
        buttonTracking = (Button) findViewById(R.id.buttonTracking);
        buttonTracking.setBackground(ContextCompat.getDrawable(this, R.drawable.tracking_on));
        buttonTracking.setAlpha(1F);

        buttonGPS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mRequestingLocationUpdates) {
                    stopLocationUpdates();
                } else {
                    createHighLocationRequest();
                    startLocationUpdates();
                }
            }
        });

        buttonTracking.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mLocationTracking) {
                    mLocationTracking = false;
                    buttonTracking.setAlpha(0.5F);
                    Toast.makeText(getApplicationContext(), "Following off", Toast.LENGTH_SHORT).show();
                } else {
                    mLocationTracking = true;
                    if (mCurrentLocation != null) {
                        setPosition(mCurrentLocation);
                    }
                    buttonTracking.setAlpha(1F);
                    Toast.makeText(getApplicationContext(), "Following current position", Toast.LENGTH_SHORT).show();
                }
            }
        });


        if (mCurrentLocation != null) {
            setPosition(mCurrentLocation);
        }

    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {  // more about this later
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    private void showGoogleErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment errorDialog = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(getString(R.string.DIALOG_ERROR), errorCode);
        errorDialog.setArguments(args);
        FragmentManager fm = getSupportFragmentManager();
        errorDialog.show(fm, "Error!");

    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == getResources().getInteger(R.integer.REQUEST_RESOLVE_ERROR)) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (mResolvingError == false) {
            if (result.hasResolution()) {
                try {
                    mResolvingError = true;
                    result.startResolutionForResult(this, getResources().getInteger(R.integer.REQUEST_RESOLVE_ERROR));
                } catch (IntentSender.SendIntentException e) {
                    // There was an error with the resolution intent. Try again.
                    mGoogleApiClient.connect();
                }
            } else {
                // Show dialog using GooglePlayServicesUtil.getErrorDialog()
                showGoogleErrorDialog(result.getErrorCode());
                mResolvingError = true;
            }
        }
    }

    private void setPosition(Location loc) {
        GeoPoint gp = new GeoPoint((int) (loc.getLatitude() * 1E6), (int) (loc.getLongitude() * 1E6));
        setPosition(gp);
    }


    private void setPosition(GeoPoint gp){

        if (mapController == null || mapView == null) {
            return;
        }

        final int distanceToOld = gp.distanceTo(mCurrentLocation);
        mCurrentLocation = gp;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

        if(distanceToOld > 0) {

            if (mLocationTracking) {
                mapController.animateTo(gp);
            }

            OverlayItem curPosItem = new OverlayItem("Current Position", gp.toString(), gp);

            SimpleLocationOverlay locaOverlay = new SimpleLocationOverlay(getApplicationContext()) {
                /* (non-Javadoc)
                * @see org.osmdroid.views.overlay.SimpleLocationOverlay#draw(android.graphics.Canvas, org.osmdroid.views.MapView, boolean)
                */
                @Override
                public void draw(Canvas c, MapView osmv, boolean shadow) {
                    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setAntiAlias(true);
                    paint.setStrokeWidth(3);
                    paint.setColor(Color.BLUE);

                    Point mapCenterPoint = new Point();
                    GeoPoint mapCenter = getMyLocation();
                    mapView.getProjection().toPixels(mapCenter, mapCenterPoint);
                    c.drawCircle(mapCenterPoint.x, mapCenterPoint.y, mCurrentAccurancy, paint);
                }
            };

            mapView.getOverlays().clear();
            // Add the overlay to the MapView
            mapView.getOverlays().set(0,locaOverlay);
            mapView.invalidate();

        }

        if (mCurrentSpeed > 1.2) {
            if(mLocationRequest.getInterval() != 2000){
                mLocationRequest.setInterval(2000);
                mLocationRequest.setFastestInterval(1900);
                restartLocationUpdates();
            }
        } else {
            if(mLocationRequest.getInterval() != 10000){
                mLocationRequest.setInterval(10000);
                mLocationRequest.setFastestInterval(9900);
                restartLocationUpdates();
            }
        }


    }



    @Override
    protected void onPause() {
        super.onPause();
        saveInstance();
        stopLocationUpdates();
    }


    @Override
    public void onResume() {
        super.onResume();

        loadInstance();

        if (mCurrentLocation != null) {
            setPosition(mCurrentLocation);
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
            createHighLocationRequest();
            startLocationUpdates();
        } else {
            toggleGpsButton(false);
        }
    }


    private void loadInstance() {
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);

        if (sharedPref.contains(getString(R.string.REQUESTING_LOCATION_UPDATES_KEY))) {
            mRequestingLocationUpdates = sharedPref.getBoolean(getString(R.string.REQUESTING_LOCATION_UPDATES_KEY), false);
        }

        if ((sharedPref.contains(getString(R.string.LOCATION_LAT))) && (sharedPref.contains(getString(R.string.LOCATION_LON)))) {

            if (mCurrentLocation == null) {
                mCurrentLocation = new GeoPoint((double) sharedPref.getFloat(getString(R.string.LOCATION_LAT), 52L), (double) sharedPref.getFloat(getString(R.string.LOCATION_LON), 13F));
            }

        }

        if (sharedPref.contains(getString(R.string.LAST_UPDATED_TIME_STRING_KEY))) {
            mLastUpdateTime = sharedPref.getString(getString(R.string.LAST_UPDATED_TIME_STRING_KEY), null);
        }

        if (mapController != null) {
            if (sharedPref.contains(getString(R.string.LAST_ZOOM_LEVEL))) {
                mapController.setZoom(sharedPref.getInt(getString(R.string.LAST_ZOOM_LEVEL), 8));
            }
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    private void saveInstance() {

        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putBoolean(getString(R.string.REQUESTING_LOCATION_UPDATES_KEY), mRequestingLocationUpdates);
        editor.putFloat(getString(R.string.LOCATION_LAT), ((float) mCurrentLocation.getLatitude()));
        editor.putFloat(getString(R.string.LOCATION_LON), ((float) mCurrentLocation.getLongitude()));
        editor.putString(getString(R.string.LAST_UPDATED_TIME_STRING_KEY), mLastUpdateTime);
        editor.putInt(getString(R.string.LAST_ZOOM_LEVEL), mapView.getZoomLevel());
        editor.apply();

    }


    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            mCurrentSpeed = location.getSpeed();
            mCurrentAccurancy = location.getAccuracy();
            setPosition(location);
        }


    }


    private void startLocationUpdates() {
        if (mGoogleApiClient.isConnected() == true && mLocationRequest != null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            mRequestingLocationUpdates = true;
            toggleGpsButton(true);
            mLocationAvailability = LocationServices.FusedLocationApi.getLocationAvailability(mGoogleApiClient);
            if (mLocationAvailability.isLocationAvailable() == false) {
                //Toast.makeText(getApplicationContext(), "Positioning not available.", Toast.LENGTH_SHORT).show();
            }
        } else {
            // Toast.makeText(getApplicationContext(), "Positioning not available.", Toast.LENGTH_SHORT).show();
            stopLocationUpdates();
        }
    }

    private void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mRequestingLocationUpdates = false;
        }
        toggleGpsButton(false);

    }

    private void restartLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    private void toggleGpsButton(boolean on) {
        if (on) {
            buttonGPS.setBackground(ContextCompat.getDrawable(this, R.drawable.gps_activated));
        } else {
            buttonGPS.setBackground(ContextCompat.getDrawable(this, R.drawable.gps_deactivated));
            buttonGPS.setAlpha(1);
        }
    }

    private void createHighLocationRequest() {

        if(mLocationRequest== null) {
            mLocationRequest = new LocationRequest();
        }
        mLocationRequest.setInterval(1100);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (buttonGPS != null) {
            buttonGPS.setAlpha(1);
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            setPosition(mLastLocation);
        }

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

}
