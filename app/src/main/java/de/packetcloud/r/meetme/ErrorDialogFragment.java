package de.packetcloud.r.meetme;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Created by R on 29.06.2015.
 */
public class ErrorDialogFragment extends DialogFragment {
    public ErrorDialogFragment() {
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Get the error code and retrieve the appropriate dialog
        int errorCode = 0;
        if (savedInstanceState != null) {
            errorCode = this.getArguments().getInt(getString(R.string.DIALOG_ERROR));
        }

        return GooglePlayServicesUtil.getErrorDialog(errorCode, this.getActivity(), getResources().getInteger(R.integer.REQUEST_RESOLVE_ERROR));
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        ((MeetActivity) getActivity()).onDialogDismissed();
    }
}
